from five import grok
from zope import schema
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice
from Products.Five import BrowserView

from plone.directives import form, dexterity
from plone.dexterity.content import Item
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.app.textfield import RichText
from plone.namedfile.field import NamedImage

try:
    from plone.app.caching.operations.utils import setCacheHeaders
except ImportError:
    setCacheHeaders = None

from tabellio.webviews import MessageFactory as _

class ISliderNews(form.Schema):
    picture = NamedImage(title=_(u'Picture'), required=False)
    text = RichText(title=_(u'Text'))
    on_homepage = schema.Bool(title=_(u'Include on homepage'), default=True)

class SliderNews(Item):
    implements(ISliderNews)

class View(grok.View):
    grok.context(ISliderNews)
    grok.require('zope2.View')

class PictureView(BrowserView):
    def __call__(self):
        self.request.response.setHeader('Content-type', self.context.picture.contentType)
        if setCacheHeaders:
            setCacheHeaders(self, self.request, self.request.response,
                            maxage=300, smaxage=300,
                            lastModified=self.context.modification_date.asdatetime())
        return self.context.picture.data
