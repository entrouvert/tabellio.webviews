from plone.portlets.interfaces import IPortletDataProvider
from zope.component import adapts, getMultiAdapter, queryUtility
from zope.formlib import form
from zope.interface import implements, Interface
from zope import schema

from Acquisition import aq_inner, aq_base, aq_parent
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.portlets.portlets import base

from tabellio.webviews import MessageFactory as _

class INavigationPortlet(IPortletDataProvider):
    """A portlet which can render the navigation tree
    """

    name = schema.TextLine(
            title=_(u"Title"),
            description=_(u"The title of the box."),
            default=u"",
            required=False)

    links = schema.Text(
            title=_(u'Links'),
            description=_('A set of links, one per line, title after |'),
            default=u'',
            required=False)


class Assignment(base.Assignment):
    implements(INavigationPortlet)
    title = _(u'Select Navigation')
    name = u""
    links = u''

    def __init__(self, name=u"", links=u""):
        self.name = name
        self.links = links

class Renderer(base.Renderer):
    def title(self):
        return self.data.name or self.data.title

    def hasName(self):
        return self.data.name

    def update(self):
        pass

    def render(self):
        return self._template()

    def links(self):
        return [x.split('|') for x in self.data.links.splitlines()]

    _template = ViewPageTemplateFile('selectnav.pt')


class AddForm(base.AddForm):
    form_fields = form.Fields(INavigationPortlet)
    label = _(u"Add <select> Navigation Portlet")
    description = _(u"This portlet display a list of links in a <select>.")

    def create(self, data):
        return Assignment(name=data.get('name', u""))


class EditForm(base.EditForm):
    form_fields = form.Fields(INavigationPortlet)
    label = _(u"Edit <select> Navigation Portlet")
    description = _(u"This portlet display a list of links in a <select>.")


