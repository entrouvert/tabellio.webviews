import datetime

from Products.Five import BrowserView
from zope.interface import Interface
from zope import component

from Products.CMFCore.utils import getToolByName

try:
    from plone.app.caching.operations.utils import setCacheHeaders
except ImportError:
    setCacheHeaders = None

from tabellio.agenda.utils import MonthlyView

class Cached:
    def cache(self, duration=30):
        if setCacheHeaders:
            setCacheHeaders(self, self.request, self.request.response,
                        maxage=duration, smaxage=duration)


class IFolderWithBottomNavView(Interface):
    pass

class FolderWithBottomNavView(BrowserView, Cached):
    pass

class IPressFolder(Interface):
    pass

class PressFolder(BrowserView, Cached):
    def getRecent(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        folder_path = '/'.join(self.context.getPhysicalPath() + ('communiques',))
        results = catalog.searchResults(path={'query': folder_path, 'depth': 1},
                portal_type=['Document', 'File'],
                sort_on='Date', sort_order='descending',
                sort_limit=10)[:10]
        return results

    def getRecentPresse(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        folder_path = '/'.join(self.context.getPhysicalPath() + ('les-medias-en-parlent',))
        results = catalog.searchResults(path={'query': folder_path, 'depth': 1},
                portal_type=['Document', 'File'],
                sort_on='Date', sort_order='descending',
                sort_limit=10)[:10]
        return results

class IPcfHomeFolder(Interface):
    pass

class PcfHomeFolder(BrowserView, Cached):
    def slider_items(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        objects = catalog(portal_type=['tabellio.webviews.slidernews'])
        if len(objects) == 0:
            return []
        now = datetime.datetime.now().timetuple()
        objects = [x.getObject() for x in objects if x.getObject().on_homepage and
                                                     (not x.expires or x.expires.parts() > now) and
                                                     (not x.effective or x.effective.parts() < now)]
        if len(objects) == 0:
            return []

        # and now, we know all items are coming from the same directory, so we
        # take it and reorder items according to their manually set position
        # in the folder.
        sliders_folder = objects[0].aq_parent
        sliders_folder_objects = list(sliders_folder.objectValues())
        def cmp_position(x, y):
            if not x in sliders_folder_objects:
                return -1
            if not y in sliders_folder_objects:
                return -1
            return cmp(sliders_folder_objects.index(x),
                       sliders_folder_objects.index(y))
        objects.sort(cmp_position)
        return objects

    def homenews(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        objects = catalog(portal_type=['tabellio.pcfviews.homenews'], sort_on='effective')
        now = datetime.datetime.now().timetuple()
        l = [x for x in objects if x.getObject().on_homepage and
                                   (not x.expires or x.expires.parts() > now) and
                                   (not x.effective or x.effective.parts() < now)]
        if not l:
           return None
        return l[0].getObject()

    def get_coming_day_events(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        # do not go more than 100 days in the future
        start = datetime.date.today()
        end = start + datetime.timedelta(100)
        calendar = getToolByName(self.context, 'portal_calendar')
        r = catalog(
                    portal_type=['tabellio.agenda.comevent',
                                 'tabellio.agenda.parlevent',
                                 'tabellio.agenda.event'],
                    review_state=calendar.getCalendarStates(),
                    start={'query': start, 'range': 'min'},
                    sort_on='start')
        if len(r) == 0:
            return None
        day = r[0].getObject().start.timetuple()[:3]
        return [x.getObject() for x in r if x.start.timetuple()[:3] == day]

    def next_event_url(self):
        try:
            current_event = self.get_coming_day_events()[-1]
        except (TypeError, IndexError):
            return None
        catalog = getToolByName(self.context, 'portal_catalog')
        # do not go more than 100 days in the future
        end = current_event.start + datetime.timedelta(100)
        start = current_event.start
        monthly_view = MonthlyView(self.context, init_calendar=True)
        serie_of_events = monthly_view.get_events_from_catalog(
                start, end, portal_type=['tabellio.agenda.parlevent',
                                         'tabellio.agenda.comevent',
                                         'tabellio.agenda.event'])
        for i, event in enumerate(serie_of_events):
            event_id = ((type(event.getId) is str) and event.getId or event.getId())
            if event_id == current_event.id:
                try:
                    return serie_of_events[i+1].getURL()
                except IndexError:
                    return None
        return None

    def previous_event_url(self):
        try:
            current_event = self.get_coming_day_events()[0]
        except (TypeError, IndexError):
            return None
        catalog = getToolByName(self.context, 'portal_catalog')
        end = current_event.start + datetime.timedelta(1)
        # do not go back more than 100 days in the past
        start = current_event.start - datetime.timedelta(100)
        monthly_view = MonthlyView(self.context, init_calendar=True)
        serie_of_events = monthly_view.get_events_from_catalog(
                start, end, portal_type=['tabellio.agenda.parlevent',
                                         'tabellio.agenda.comevent',
                                         'tabellio.agenda.event'])
        for i, event in enumerate(serie_of_events):
            event_id = ((type(event.getId) is str) and event.getId or event.getId())
            if event_id == current_event.id:
                if i == 0:
                    return None
                return serie_of_events[i-1].getURL()
        return None

    def last_docs(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        return catalog(
                portal_type=['tabellio.documents.document'],
                sort_on='publication_date', sort_order='descending', limit=10)[:5]

    def more_docs_link(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            return catalog(
                portal_type='Folder',
                object_provides='tabellio.webviews.misc.IFolderWithRecentDocs',
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return '#'

class IComiteMixteFolder(Interface):
    pass

class ComiteMixteFolder(BrowserView, Cached):
    pass


class IFolderWithRecentDocs(Interface):
    pass

class FolderWithRecentDocs(BrowserView, Cached):
    def last_docs(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        return catalog(
                portal_type=['tabellio.documents.document'],
                sort_on='publication_date', sort_order='descending', limit=100)[:50]

class IFolderNoNoNav(Interface):
    pass

class FolderNoNoNav(BrowserView, Cached):
    pass

