import datetime

from zope import component

from plone.portlets.interfaces import IPortletDataProvider
from zope.component import adapts, getMultiAdapter, queryUtility
from zope.formlib import form
from zope.interface import implements, Interface
from zope import schema

from Acquisition import aq_inner, aq_base, aq_parent
from Products.CMFCore.utils import getToolByName
from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.portlets.portlets import base

from tabellio.webviews import MessageFactory as _

class IStreamingPortlet(IPortletDataProvider):
    """A portlet to render a link to the stream
    """

    name = schema.TextLine(
            title=_(u"Title"),
            description=_(u"The title of the box."),
            default=u"",
            required=False)


class Assignment(base.Assignment):
    implements(IStreamingPortlet)
    title = _(u'Streaming Link')
    name = u""

    def __init__(self, name=u""):
        self.name = name

class Renderer(base.Renderer):
    def title(self):
        return self.data.name or self.data.title

    def hasName(self):
        return self.data.name

    def update(self):
        pass

    def render(self):
        return self._template()

    def current_meeting(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        yesterday = datetime.datetime.now() - datetime.timedelta(1)
        now = datetime.datetime.now()
        r = catalog(
                    portal_type=['tabellio.agenda.parlevent'],
                    start={'query': yesterday, 'range': 'min'},
                    sort_on='start')
        if len(r) == 0:
            return None
        for e in r:
            event = e.getObject()
            if event.end and event.end < now:
                continue
            if event.start and event.start > now:
                return None
            return event
        return None

    def live_url(self):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if settings.embedded_audio_player_url:
            return settings.embedded_audio_player_url
        return self.current_meeting.absolute_url() + '/ecouter.m3u'

    def next_meeting(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        # do not go more than 30 days in the future
        now = datetime.datetime.now()
        r = catalog(
                    portal_type=['tabellio.agenda.parlevent'],
                    start={'query': now, 'range': 'min'},
                    sort_on='start')
        if len(r) == 0:
            return None
        for e in r:
            event = e.getObject()
            if event.end and event.end < now:
                continue
            return event
        return None

    _template = ViewPageTemplateFile('streaming.pt')


class AddForm(base.AddForm):
    form_fields = form.Fields(IStreamingPortlet)
    label = _(u"Add Streaming Link Portlet")
    description = _(u"This portlet display a link to the stream.")

    def create(self, data):
        return Assignment(name=data.get('name', u""))


class EditForm(base.EditForm):
    form_fields = form.Fields(IStreamingPortlet)
    label = _(u"Edit Streaming Link Portlet")
    description = _(u"This portlet display a link to the stream.")

